$(function(){
  // var pageHeight = $(document).height();
  // $('.page-head--js').css('height', pageHeight+'px');

  $('.parallax--js').each(function(){
    	var $obj = $(this);

    	$(window).scroll(function() {
    		var yPos = -($(window).scrollTop() / $obj.data('speed'));

    		var bgpos = 'center '+ yPos + 'px';

    		$obj.css('background-position', bgpos );

    	});
    });

    $('.filter-category-action--js').on('click', function(){
      $('.filter-options').toggleClass('opened');
    })

    // $(".filter--js").sticky({topSpacing:0});

    $(".filter-options-btn--js").on('click', function(){
      var tagValue = $(this).data('value');
      console.log(tagValue);
      $(this).toggleClass('selected');
      if (!$('span[data-tag="'+tagValue+'"]').length) {
        var $tag = $('<span>').html(tagValue).attr('data-tag',tagValue);
        $('.filter-options-tags-items--js').append($tag);
      } else{
        $('span[data-tag="'+tagValue+'"]').remove();
      }
    })

})


$('.form-simulation-value--js').priceFormat({
  prefix: 'R$ ',
  centsSeparator: ',',
  thousandsSeparator: '.',
  limit: 10
});

$('.form-simulation-value--inline--js').priceFormat({
  prefix: 'R$ ',
  centsSeparator: ',',
  thousandsSeparator: '.',
  limit: 10
}).keyup(valorLinhaResize).each(valorLinhaResize);

$('.form-simulation-time--js').selectric();

function valorLinhaResize() {
  $(this).css('width', ($(this).val().length * 16) + 'px');
}
$('#carousel--js, #carousel-2--js').slick({
  infinite: false,
  slidesToShow: 2.5,
  // slidesToScroll: 2,
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 1.2,
      slidesToScroll: 1
    }
  }]
});

$(window).scroll(function() {
  $( '.svg-container:in-viewport(-300)').addClass('animation')
})

// $(".banner-full").waypoint(function() {
//     $(".svg-container").addClass('animation');
//     console.log('asdsdsaadsds');
//   },
//   { offset: '400'});
